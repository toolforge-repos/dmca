## dmca
A technical utility built by the Wikimedia Foundation Trust & Safety team. This project is based on the Toolforge Flask + OAuth WSGI tutorial.

This tool is used to facilitate the removal of content for Legal compliance reasons. For more information on the process, you can visit (Wikimedia Commons' DMCA noticeboard)[https://commons.wikimedia.org/wiki/Commons:Office_actions/DMCA_notices].

## Sync folder
Run the following command to make sure `nodemon` pushes every changes
`FLASK_ENV=development && nodemon -e js,css,html,py -x "flask run --reload --port=5000"`

## Documentation
* https://github.com/remy/nodemon
