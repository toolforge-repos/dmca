"use strict"

$(document).ready(function() {
    var file_name = "Delete.JPG"; //dummy for testing

    class Utils {
        /**
         * Get Homeurl
         */
        getBaseUrl() {
            // use home url link as baseurl, remove protocol
            var baseUrl = document.getElementById('baseurl').getAttribute('href').replace(/^https?:\/\//,'');
            // add actual protocol to fix Flask bug with protocol inconsistency
            // baseUrl = location.protocol + '//' + baseUrl
            return baseUrl;
        }
    }

    // form validation
    var forms = document.querySelectorAll('.needs-validation');
    // loop over them and prevent submission
    Array.prototype.slice.call(forms)
        .forEach(function (form) {
            form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                    event.preventDefault()
                    event.stopPropagation()
                }
                form.classList.add('was-validated');
            }, false)
    })

    // country list
    var countries = [["us","United States"],["af","Afghanistan"],["ax","Åland Islands"],["al","Albania"],["dz","Algeria"],["as","American Samoa"],["ad","Andorra"],["ao","Angola"],["ai","Anguilla"],["aq","Antarctica"],["ag","Antigua and Barbuda"],["ar","Argentina"],["am","Armenia"],["aw","Aruba"],["au","Australia"],["at","Austria"],["az","Azerbaijan"],["bs","Bahamas"],["bh","Bahrain"],["bd","Bangladesh"],["bb","Barbados"],["by","Belarus"],["be","Belgium"],["bz","Belize"],["bj","Benin"],["bm","Bermuda"],["bt","Bhutan"],["bo","Bolivia"],["bq","Bonaire, Sint Eustatius and Saba"],["ba","Bosnia and Herzegovina"],["bw","Botswana"],["bv","Bouvet Island"],["br","Brazil"],["io","British Indian Ocean Territory"],["bn","Brunei Darussalam"],["bg","Bulgaria"],["bf","Burkina Faso"],["bi","Burundi"],["cv","Cabo Verde"],["kh","Cambodia"],["cm","Cameroon"],["ca","Canada"],["ky","Cayman Islands"],["cf","Central African Republic"],["td","Chad"],["cl","Chile"],["cn","China"],["cx","Christmas Island"],["cc","Cocos (Keeling) Islands"],["co","Colombia"],["km","Comoros"],["cd","Congo, The Democratic Republic of the"],["cg","Congo"],["ck","Cook Islands"],["cr","Costa Rica"],["ci","Côte d&#39;Ivoire"],["hr","Croatia"],["cu","Cuba"],["cw","Curaçao"],["cy","Cyprus"],["cz","Czechia"],["dk","Denmark"],["dj","Djibouti"],["dm","Dominica"],["do","Dominican Republic"],["ec","Ecuador"],["eg","Egypt"],["sv","El Salvador"],["gq","Equatorial Guinea"],["er","Eritrea"],["ee","Estonia"],["sz","Eswatini"],["et","Ethiopia"],["fk","Falkland Islands (Malvinas)"],["fo","Faroe Islands"],["fj","Fiji"],["fi","Finland"],["fr","France"],["gf","French Guiana"],["pf","French Polynesia"],["tf","French Southern Territories"],["ga","Gabon"],["gm","Gambia"],["ge","Georgia"],["de","Germany"],["gh","Ghana"],["gi","Gibraltar"],["gr","Greece"],["gl","Greenland"],["gd","Grenada"],["gp","Guadeloupe"],["gu","Guam"],["gt","Guatemala"],["gg","Guernsey"],["gw","Guinea-Bissau"],["gn","Guinea"],["gy","Guyana"],["ht","Haiti"],["hm","Heard Island and McDonald Islands"],["va","Holy See (Vatican City State)"],["hn","Honduras"],["hk","Hong Kong"],["hu","Hungary"],["is","Iceland"],["in","India"],["id","Indonesia"],["ir","Iran"],["iq","Iraq"],["ie","Ireland"],["im","Isle of Man"],["il","Israel"],["it","Italy"],["jm","Jamaica"],["jp","Japan"],["je","Jersey"],["jo","Jordan"],["kz","Kazakhstan"],["ke","Kenya"],["ki","Kiribati"],["kw","Kuwait"],["kg","Kyrgyzstan"],["la","Lao People&#39;s Democratic Republic"],["lv","Latvia"],["lb","Lebanon"],["ls","Lesotho"],["lr","Liberia"],["ly","Libya"],["li","Liechtenstein"],["lt","Lithuania"],["lu","Luxembourg"],["mo","Macao"],["mg","Madagascar"],["mw","Malawi"],["my","Malaysia"],["mv","Maldives"],["ml","Mali"],["mt","Malta"],["mh","Marshall Islands"],["mq","Martinique"],["mr","Mauritania"],["mu","Mauritius"],["yt","Mayotte"],["mx","Mexico"],["fm","Micronesia, Federated States of"],["md","Moldova"],["mc","Monaco"],["mn","Mongolia"],["me","Montenegro"],["ms","Montserrat"],["ma","Morocco"],["mz","Mozambique"],["mm","Myanmar"],["na","Namibia"],["nr","Nauru"],["np","Nepal"],["nl","Netherlands"],["nc","New Caledonia"],["nz","New Zealand"],["ni","Nicaragua"],["ne","Niger"],["ng","Nigeria"],["nu","Niue"],["nf","Norfolk Island"],["kp","North Korea"],["mk","North Macedonia"],["mp","Northern Mariana Islands"],["no","Norway"],["om","Oman"],["pk","Pakistan"],["pw","Palau"],["ps","Palestine, State of"],["pa","Panama"],["pg","Papua New Guinea"],["py","Paraguay"],["pe","Peru"],["ph","Philippines"],["pn","Pitcairn"],["pl","Poland"],["pt","Portugal"],["pr","Puerto Rico"],["qa","Qatar"],["re","Réunion"],["ro","Romania"],["ru","Russian Federation"],["rw","Rwanda"],["bl","Saint Barthélemy"],["sh","Saint Helena, Ascension and Tristan da Cunha"],["kn","Saint Kitts and Nevis"],["lc","Saint Lucia"],["mf","Saint Martin (French part)"],["pm","Saint Pierre and Miquelon"],["vc","Saint Vincent and the Grenadines"],["ws","Samoa"],["sm","San Marino"],["st","Sao Tome and Principe"],["sa","Saudi Arabia"],["sn","Senegal"],["rs","Serbia"],["sc","Seychelles"],["sl","Sierra Leone"],["sg","Singapore"],["sx","Sint Maarten (Dutch part)"],["sk","Slovakia"],["si","Slovenia"],["sb","Solomon Islands"],["so","Somalia"],["za","South Africa"],["gs","South Georgia and the South Sandwich Islands"],["kr","South Korea"],["ss","South Sudan"],["es","Spain"],["lk","Sri Lanka"],["sd","Sudan"],["sr","Suriname"],["sj","Svalbard and Jan Mayen"],["se","Sweden"],["ch","Switzerland"],["sy","Syrian Arab Republic"],["tw","Taiwan"],["tj","Tajikistan"],["tz","Tanzania"],["th","Thailand"],["tl","Timor-Leste"],["tg","Togo"],["tk","Tokelau"],["to","Tonga"],["tt","Trinidad and Tobago"],["tn","Tunisia"],["tr","Türkiye"],["tm","Turkmenistan"],["tc","Turks and Caicos Islands"],["tv","Tuvalu"],["ug","Uganda"],["ua","Ukraine"],["ae","United Arab Emirates"],["gb","United Kingdom"],["um","United States Minor Outlying Islands"],["uy","Uruguay"],["uz","Uzbekistan"],["vu","Vanuatu"],["ve","Venezuela"],["vn","Vietnam"],["vg","Virgin Islands, British"],["vi","Virgin Islands, U.S."],["wf","Wallis and Futuna"],["eh","Western Sahara"],["ye","Yemen"],["zm","Zambia"],["zw","Zimbabwe"]];
    for (var i=0;i<countries.length;i++) {
        $("#sender_country").append("<option value='" + countries[i][0] + "'>" + countries[i][1] + "</option>");
    };

    // copy and paste
    $(".copy").on("click", function(){
        var number = $(".copy").index(this);
        var selector = $('.form-control').eq(number);
        var copyText = selector.val();
        navigator.clipboard.writeText(copyText);
        $(selector)
            .animate({"background-color": "#cc88cc"}, 500)
            .delay(200)
            .animate({"background-color": "#ffffff"}, 500);
    });

    $("#get-image-details").on("click", function() {
        const file_url = $("#file_url").val();
        if (file_url.length == 0) {
            alert("Please provide a URL");
        } else {
            var full_file_name = decodeURI(file_url.replace("https://commons.wikimedia.org/wiki/", ""));
            var file_name = decodeURI(file_url.replace("https://commons.wikimedia.org/wiki/File:", ""));
            $("#file_name").val(file_name.replace(/_/g, " "));
            var dmca_title = file_name.replace(/\.(jpg|png|svg|pdf|tiff?|gif|webm|ogg)$/i, "").replace(/_/g, " ");
            $("#dmca_title").val(dmca_title);

            const {query} = $.ajax( {
                type: "GET",
                url: 'https://commons.wikimedia.org/w/api.php',
                data: {
                    action: 'query',
                    prop: 'imageinfo',
                    format: 'json',
                    origin: '*',
                    titles: full_file_name,
                    iiprop: 'user'
                },
                xhrFields: {
                    withCredentials: false
                },
                dataType: 'json',
                success: function(data) {
                    var pageID = Object.keys(data.query.pages)[0];
                    var pathway = "data.query.pages." + pageID + ".imageinfo[0].user";
                    var uploader = data["query"]["pages"][pageID]["imageinfo"];
                    uploader = uploader[0].user;
                    $("#user_name").val(uploader);
                },
                error: function() {
                    alert("JSON error :(");
                }
            });
        }
    });
    
});

// dummy fill
    $("#dummy-fill").on("click", function() {
        $("#file_url").val("https://commons.wikimedia.org/wiki/File:Example.jpg");
        $("#file_name").val("Example.jpg");
        $("#user_name").val("Foo");
        $("#project_name").val("Wikimedia Commons");
        $("#dmca_title").val("Bar");
        $("#subject").val("Baz");
        $("#sent_date").val("2023-01-01");
        $("#received_date").val("2023-01-01");
        $("#email_manner").val("email");
        $("#email_content").val("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");
        $("#original_work_url").val("https://www.example.com/");
        $("#sender_name").val("Joe Bloggs");
        $("#sender_address1").val("123 Street Rd");
        $("#sender_address2").val("Suite 456");
        $("#sender_city").val("Anytown");
        $("#sender_state").val("Pennsylvania");
        $("#sender_zip_code").val("12345");
        $("#sender_phone").val("1234567890");
        $("#sender_email").val("fake@example.com");
    });