#!/usr/bin/env python3

# Author: Joe Sutherland, heavily based on code by Samuel Guebo
# Description: Utility for generating a list of
# users in wikicode text. It is used to populate
# m:Access to nonpublic personal data policy/Noticeboard
# License: MIT

from flask import render_template
from flask import request
from flask import Blueprint
from utils import app
#from datetime import datetime
import urllib.parse

takedown = Blueprint('takedown', __name__)

sent_date = "" # Declaring this here since it is used in two functions

@takedown.route('/takedown',  methods=['GET', 'POST'])
def index(title=None):
    """
    Default/Home page of the application/tool.

    Keyword arguments:
    title -- the default title of the homepage
    """

    title = "DMCA takedown"
    description = "Tool to accommodate DMCA takedowns."
    noticeboard_url = app.config["NOTICEBOARD_URL"]
    wmf_url = app.config["WMF_URL"]
    vp_url = app.config["VP_URL"]

    if request.method == 'POST':
        # gather all data from the form
        # TODO: probably these dummy data aren't necessary
        if "file_name" in request.form:
            file_name = request.form['file_name']
        else:
            file_name = ""
        if "user_name" in request.form:
            user_name = request.form['user_name']
        else:
            user_name = ""
        if "project_name" in request.form:
            project_name = request.form['project_name']
        else:
            project_name = "Wikimedia Commons"
        if "dmca_title" in request.form:
            dmca_title = request.form['dmca_title']
        else:
            dmca_title = ""
        if "subject" in request.form:
            subject = request.form['subject']
        else:
            subject = ""
        if "sent_date" in request.form:
            sent_date = request.form['sent_date']
        else:
            sent_date = ""
        if "received_date" in request.form:
            received_date = request.form['received_date']
        else:
            received_date = ""
        if "email_manner" in request.form:
            email_manner = request.form['email_manner']
        else:
            email_manner = "email"

        if "email_content" in request.form:
            email_content = request.form['email_content']
        else:
            email_content = ""
        if "action_taken" in request.form:
            action_taken = request.form['action_taken']
        else:
            action_taken = "Yes"

        if "original_work_url" in request.form:
            original_work_url = request.form['original_work_url']
        else:
            original_work_url = ""

        if "sender_name" in request.form:
            sender_name = request.form['sender_name']
        else:
            sender_name = ""
        if "sender_type" in request.form:
            sender_type = request.form['sender_type']
        else:
            sender_type = ""
        if "sender_address1" in request.form:
            sender_address1 = request.form['sender_address1']
        else:
            sender_address1 = ""
        if "sender_address2" in request.form:
            sender_address2 = request.form['sender_address2']
        else:
            sender_address2 = ""
        if "sender_city" in request.form:
            sender_city = request.form['sender_city']
        else:
            sender_city = ""
        if "sender_state" in request.form:
            sender_state = request.form['sender_state']
        else:
            sender_state = ""
        if "sender_zip_code" in request.form:
            sender_zip_code = request.form['sender_zip_code']
        else:
            sender_zip_code = ""
        if "sender_country" in request.form:
            sender_country = request.form['sender_country']
        else:
            sender_country = ""
        if "sender_phone" in request.form:
            sender_phone = request.form['sender_phone']
        else:
            sender_phone = ""
        if "sender_email" in request.form:
            sender_email = request.form['sender_email']
        else:
            sender_email = ""

        # ...
        # TODO: Send this all to Lumen with a POST request
        # ...

        user_name_enc = urllib.parse.quote(user_name)

        return render_template(
            'takedown-notices.html', title=title,
            description=description, noticeboard_url=noticeboard_url,
            wmf_url=wmf_url, vp_url=vp_url, 
            file_name=file_name, user_name=user_name, project_name=project_name, dmca_title=dmca_title, subject=subject, sent_date=sent_date, received_date=received_date, email_manner=email_manner, email_content=email_content, action_taken=action_taken, original_work_url=original_work_url, sender_name=sender_name, sender_type=sender_type, sender_address1=sender_address1, sender_address2=sender_address2, sender_city=sender_city, sender_state=sender_state, sender_zip_code=sender_zip_code, sender_country=sender_country, sender_phone=sender_phone, sender_email=sender_email,user_name_enc=user_name_enc,
            foundation_wiki_text=gen_foundation_notice(email_content, sent_date, subject, sender_email, file_name), noticeboard_text=gen_noticeboard_text(dmca_title,file_name),village_pump_text=gen_village_pump_text(dmca_title,file_name), user_notice=gen_user_notice(user_name,dmca_title,file_name)
        )
    else:
        print("the 'else' thing happened")

    return render_template(
        'takedown.html', title=title,
        description=description, remote_wiki_text="foobar",
        noticeboard_url=noticeboard_url
    )


def gen_foundation_notice(content, date, subject, email, file):
    """
    Generate a copy of the DMCA for Foundationwiki

    Keyword arguments:
    content -- content of the email / takedown notice
    date -- date the notice was sent
    subject -- subject line of the takedown email
    email -- email from which the notice was sent
    file -- affected (deleted) file
    """

    output = ""
    # add metadata
    output += "{{DMCA email\n|from=" + email + "\n|date=" + date + "\n|subject=" + subject + "\n|to=legal@wikimedia.org\n|content=[[c:File:" + file + "]]\n|message=<nowiki>\n"
    # add content
    output += content
    # close it out with the year
    #dateString = datetime.strptime(sent_date, "%Y-%m-%d")
    #output += "</nowiki>\n| year = " + str(dateString.year) + "}}"
    output += "</nowiki>\n| year = 2023}}"

    return output


def gen_noticeboard_text(title, file):
    """
    Generate text for [[Commons:Office actions/DMCA notices]]

    Keyword arguments:
    title -- title of the DMCA, used to submit to Lumen and foundationwiki
    file -- name of the affected file
    """

    output = ""
    # add DMCA blurb
    output += "In compliance with the provisions of the US [[:en:Digital Millennium Copyright Act|Digital Millennium Copyright Act]] (DMCA), and at the instruction of the [[Wikimedia Foundation]]'s legal counsel, one or more files have been deleted from Commons. Please note that this is an [[Commons:Office actions|official action of the Wikimedia Foundation office]] which should not be undone. If you have valid grounds for a counter-claim under the DMCA, please contact me.\n\n"
    # add link to takedown on foundationwiki
    output += "The takedown can be read [[:wmf:Legal:DMCA " + title + "|'''here''']].\n\n"
    # add affected file link
    output += "Affected file(s):\n* [[:File: " + file + "]]\n\n"
    # sign
    output += "Thank you! ~~~~"

    return output


def gen_village_pump_text(title, file):
    """
    Generate text for [[Commons:Village pump]]

    Keyword arguments:
    title -- title of the DMCA, used to submit to Lumen and foundationwiki
    file -- name of the affected file
    """

    output = ""
    # add DMCA blurb
    output += "In compliance with the provisions of the US [[:en:Digital Millennium Copyright Act|Digital Millennium Copyright Act]] (DMCA), and at the instruction of the [[Wikimedia Foundation]]'s legal counsel, one or more files have been deleted from Commons. Please note that this is an [[Commons:Office actions|official action of the Wikimedia Foundation office]] which should not be undone. If you have valid grounds for a counter-claim under the DMCA, please contact me.\n\n"
    # add link to takedown on foundationwiki
    output += "The takedown can be read [[:wmf:Legal:DMCA " + title + "|'''here''']].\n\n"
    # add affected file link
    output += "Affected file(s):\n* {{lf|" + file + "}}\n\n"
    # sign
    output += "To discuss this DMCA takedown, please go to [[COM:DMCA#" + title + "]]. Thank you! ~~~~"

    return output


def gen_user_notice(user, title, file):
    """
    Generate notice for user

    Keyword arguments:
    user -- username of the uploader
    title -- title of the DMCA, used to submit to Lumen and foundationwiki
    file -- name of the affected file
    """

    output = ""
    # add DMCA blurb
    output += "Dear " + user + ",\n\nThe Wikimedia Foundation (“Wikimedia”) has taken down content that you posted at [[:File:" + file + "]] due to Wikimedia’s receipt of [[wmf:Legal:DMCA " + title + "|a validly formulated notice]] that your posted content was infringing an existing copyright.\n\n'''What Can You Do?'''\n\nYou are not obligated to take any action. However, if you feel that your content does not infringe upon any copyrights, you may contest the take down request by submitting a 'counter notice' to Wikimedia. Before doing so, you should understand your legal position, and you may wish to consult with an attorney.\n\n'''Filing a Counter Notice'''\n\nIf you choose to submit a counter notice, you must send a letter asking Wikimedia to restore your content to [mailto:legal@wikimedia.org legal@wikimedia.org], or to our service processor at the following address:  Wikimedia Foundation, c/o CT Corporation System, 818 West Seventh Street, Los Angeles, California, 90017. The letter must comply with DMCA standards, set out in Section (g)(3)(A-D), and must contain the following:\n\n* A link to where the content was before we took it down and a description of the material that was removed;\n* A statement, under penalty of perjury, that you have a good faith belief that the content was removed or disabled as a result of mistake or misidentification of the material to be removed or disabled;\n* Your name, address, and phone number;\n* If your address is in the United States, a statement that says “I consent to the jurisdiction of the Federal District Court for the district where my address is located, and I will accept service of process from the person who complained about the content I posted”; alternatively, if your address is outside the United States, a statement that says “I agree to accept service of process in any jurisdiction where the Wikimedia Foundation can be found, and I will accept service of process from the person who complained about the content I posted”; and finally,\n* Your physical or electronic signature.\n\nPursuant to the DMCA, Wikimedia must inform the alleged copyright holder that you sent us a counter notice, and give the alleged copyright holder a copy of the counter notice. The alleged copyright holder will then have fourteen (14) business days to file a lawsuit against you to restrain Wikimedia from reposting the content. If Wikimedia does not receive proper notification that the alleged copyright holder has initiated such a lawsuit against you, we will repost your content within ten (10) to fourteen (14) business days.\n\n'''Miscellaneous'''\n\nAs a matter of policy and under appropriate circumstances, Wikimedia will block the accounts of repeat infringers as provided by Section 512(i)(1)(A) of the DMCA.\n\nIf you would like to learn more about Wikimedia’s policies, please refer to the Wikimedia Terms of Use, available at [[wmf:Terms_of_use|Terms of use]], and the Wikimedia Legal Policies, available at [[m:Legal/Legal_Policies]]. More information on DMCA compliance may also be found at:\n\n* [https://lumendatabase.org/topics/29 https://lumendatabase.org/topics/29]\n* [https://www.eff.org/issues/dmca https://www.eff.org/issues/dmca]\n* [http://www.copyright.gov/onlinesp/ http://www.copyright.gov/onlinesp/]\n\nWikimedia appreciates your support. Please do not hesitate to contact us if you have any questions regarding this notice.\n\n"
    # sign
    output += "Sincerely, ~~~~"

    return output